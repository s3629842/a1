/**
 * Gary David Hill
 * s3629842
 */
#include "main.h"

#include "shared.h"
#include <limits.h>
#include <errno.h>
#include <ctype.h>
#include "options.h"

/**
 * extracts the seed (a long int) from a string passed in
 **/
static long get_seed(const char strseed[]) {
    char* end;
    long result;

#define DECIMAL 10

    /* try to extract a long integer */
    result = strtol(strseed, &end, DECIMAL);

    /* check it is within the valid range */
    if (errno == ERANGE) {
        fprintf(stderr, "Error: %s is outside the allowed "
                        " range for a long integer.\n", strseed);
        exit(EXIT_FAILURE);
    }

    /* search for any trailing characters */
    while (isspace(*end)) {
        ++end;
    }

    if (*end) {
        fprintf(stderr, "Error: %s is not a valid seed.\n", strseed);
        exit(EXIT_FAILURE);
    }

    return result;
}

enum {
    MIN_ARGS = 1,
    CORRECT_ARGS,
    OVERFLOW_ARGS
};

#define SEED_ARG 1

/**
 * you are expected to READ the comments here and fill in your code BETWEEN the
 * comments.
 **/
int main(int argc, char* argv[]) {
    enum menu_choice choice;
    menu themenu;
    enum boolean quit = FALSE;

    long seed;

    if (argc < MIN_ARGS || argc >= OVERFLOW_ARGS) {
        fprintf(stderr, "Error: invalid arguments passed in. You "
                                "should call this program as follows: ");
        fprintf(stderr, "\tcpt_220_menu [seed]\n");
        fprintf(stderr, "Where seed is an optional argument which is a "
                                "positive integer seed for the random number "
                                "generator.\n");
        return EXIT_FAILURE;
    }

    seed = argc == CORRECT_ARGS ? get_seed(argv[SEED_ARG]) : EOF;

    init_menu(themenu);

    while (!quit) {
        display_menu(themenu);

        choice = select_menu_item(themenu);

        switch (choice) {
            case MNU_REV:
            case MNU_GUESS:
            case MNU_FOLD:
            case MNU_TICTACTOE:
            case MNU_CHG:
            case MNU_HELP:
                menu_process_choice(choice);
                break;
            case MNU_QUIT:
                quit = TRUE;
                break;
            case MNU_INVAL:
                error_output("Invalid selection\n\n");
                break;
            default:
                /* Not sure what to put here. */
                break;
        }
    }

    return EXIT_SUCCESS;
}
