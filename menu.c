/**
 * Gary David Hill
 * s3629842
 */
#include "menu.h"

#include <stdarg.h>
#include <string.h>

#include "helpers.h"
#include "options.h"
#include "shared.h"

/* Empties the input buffer so we can continue. */
static void read_rest_of_line(void) {
    int ch;
    while (ch = getc(stdin), ch != '\n' && ch != EOF);
    clearerr(stdin);
}

/* Builds the menu. */
void init_menu(char menu[NUM_MENU_ITEMS][SCREENWIDTH + 1]) {
    char menu_strings[NUM_MENU_ITEMS][SCREENWIDTH + 1] = {
        "reverse a string",
        "play guess a number",
        "fold a string",
        "validate tictactoe winner",
        "check if change can be given",
        "access the help menu",
        "quit the program"
    };

    int i;

    for (i = 0; i < NUM_MENU_ITEMS; i++) {
        strcpy(menu[i], menu_strings[i]);
    }
}

/* Displays the menu. */
void display_menu(char menu[NUM_MENU_ITEMS][SCREENWIDTH + 1]) {
    int i;

    normal_output("Welcome to my menu\n");
    normal_output("==================\n");

    for (i = 0; i < NUM_MENU_ITEMS; i++) {
        normal_output("%d) %s\n", i + 1, menu[i]);
    }

    normal_output("Please enter your choice:\n");
}

/* Receives and validates menu selection input. */
enum menu_choice select_menu_item(char menu[NUM_MENU_ITEMS][SCREENWIDTH + 1]) {
    char input_c[1 + EXTRACHARS];
    char *endptr;
    int input_i;

    fgets(input_c, 1 + EXTRACHARS, stdin);

    /* Check if input is the right amount of characters. */
    if (input_c[strlen(input_c) - 1] != '\n') {
        error_output("Buffer overflow\n\n");
        read_rest_of_line();
        return MNU_INVAL;
    }

    /* Remove '\n'. */
    input_c[strlen(input_c) - 1] = 0;

    /* Convert char to long. */
    input_i = strtol(input_c, &endptr, 10);

    if (*endptr != '\0') {
        error_output("Input is not numeric\n\n");
        return MNU_INVAL;
    }

    if (input_i < MNU_MIN - 1 || input_i > MNU_MAX) {
        input_i = 0;
    }

    return input_i - 1;
}

/* Call the appropriate function for the menu choice selected. */
void menu_process_choice(enum menu_choice selected_choice) {
    if (selected_choice == MNU_REV) {
        char str[SCREENWIDTH + 1];
        reverse_string(str);
    }

    if (selected_choice == MNU_GUESS) {
        guess_a_number();
    }

    if (selected_choice == MNU_FOLD) {
        normal_output("fold\n");
    }

    if (selected_choice == MNU_TICTACTOE) {
        normal_output("ttt\n");
    }

    if (selected_choice == MNU_CHG) {
        normal_output("chg\n");
    }

    if (selected_choice == MNU_HELP) {
        display_help();
    }
}

/* Regular output. */
int normal_output(const char format[], ...) {
    va_list vl;
    int result;
    va_start(vl, format);
    result = vprintf(format, vl);
    va_end(vl);
    return result;
}

/* Display message(s) prefixed with 'Error:'. */
int error_output(const char format[], ...) {
    va_list vl;
    int result = 0;
    va_start(vl, format);
    result += fprintf(stderr, "Error: ");
    result = vfprintf(stderr, format, vl);
    va_end(vl);
    return result;
}
