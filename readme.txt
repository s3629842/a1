Author
======
Gary David Hill
s3629842 at student dot rmit dot edu dot au

Build
=====
Build command: gcc -ansi -Wall -pendantic main.c options.c helpers.c menu.c -o cpt222_menu_system

Run
===
Run in the directory where the build was created: ./cpt222_menu_system

Implementation Notes
====================
The use of the pre-defined functions has been tricky. I imagine it would be easier to create your own from scratch without having to use someone else's half-built work. I will be moving the main while loop out of the main function in order to further encapsulate functionality, as well as creating validate functions.